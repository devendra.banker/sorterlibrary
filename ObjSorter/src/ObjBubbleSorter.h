#ifndef OBJBUBBLESORTER_H
#define OBJBUBBLESORTER_H

#include <QTimer>

#include "IObjSorter.h"
#include "ObjSorter_global.h"

namespace ObjSorterspace {
class ObjBubbleSorter : public IObjSorter {
  Q_OBJECT
 public:
  ObjBubbleSorter();
  int *sort(int arr[], int size, int &errorCode) override;
  void sortInteractive(int arr[], int size, int &errorCode) override;
  void setNextIterationFlag(bool nextIterationFlag) override;

 private slots:
  void OnTimeout();

 private:
  bool compare(int x, int y);
  int *inputArray_Int;
  int inputArraySize;
  int currentIteration;
  QTimer Timer;
  bool swapFlag;
  bool nextIterationFlag;
  int outerLoopIndex;
  int innerLoopIndex;
};
}  // namespace ObjSorterspace
#endif
