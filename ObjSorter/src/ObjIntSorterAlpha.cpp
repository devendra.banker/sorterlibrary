#include "ObjIntSorterAlpha.h"

#include <iostream>

using namespace std;
using namespace ObjSorterspace;

ObjIntSorterAlpha::ObjIntSorterAlpha() {
  s = sp.getSorterStrategy(BUBBLE_SORT);
  qRegisterMetaType<ObjSorterspace::Sorter_State_t>(
      "ObjSorterspace::Sorter_State_t");
  connect(s, SIGNAL(iterationStatus(int, int, int, bool)), this,
          SIGNAL(iterationStatus(int, int, int, bool)));
  connect(s, SIGNAL(compareStatus(int, int, int, int)), this,
          SIGNAL(compareStatus(int, int, int, int)));
  connect(s, SIGNAL(sortStatus(ObjSorterspace::Sorter_State_t)), this,
          SIGNAL(sortStatus(ObjSorterspace::Sorter_State_t)));
}

int *ObjIntSorterAlpha::sort(int arr[], int size, int &errorCode) {
  s->sort(arr, size, errorCode);
  return arr;
}

void ObjIntSorterAlpha::sortInteractive(int arr[], int size, int &errorCode) {
  s->sortInteractive(arr, size, errorCode);
}

void ObjIntSorterAlpha::setNextIterationFlag(bool nextIterationFlag) {
  s->setNextIterationFlag(nextIterationFlag);
  ;
}

