#include "ObjQuickSorter.h"

#include <iostream>

using namespace std;
using namespace ObjSorterspace;

ObjQuickSorter::ObjQuickSorter()
    : inputArray_Int(nullptr),
      inputArraySize(0),
      currentIteration(0),
      nextIterationFlag(false){}

int *ObjQuickSorter::sort(int arr[], int size, int &errorCode) {
  errorCode = SORTER_SUCCESS;

  if (size > SORTER_SIZE) {
    errorCode = SORTER_FAILURE_SIZE_OVERFLOW;
    inputArray_Int = NULL;
  } else {
    inputArray_Int = (int *)malloc(sizeof(int) * size);

    quickSort(arr, 0, size - 1);

    for (int i = 0; i < size; i++) {
      inputArray_Int[i] = arr[i];
    }
    inputArraySize = size;
  }

  return inputArray_Int;
}

void ObjQuickSorter::sortInteractive(int arr[], int size, int &errorCode) {
  Q_UNUSED(arr);
  Q_UNUSED(size);
  errorCode = SORTER_INVALID;
}

void ObjQuickSorter::setNextIterationFlag(bool nextIterationFlag) {
  this->nextIterationFlag = nextIterationFlag;
}

void ObjQuickSorter::quickSort(int arr[], int low, int high) {
  if (low < high) {
    int pi = partition(arr, low, high);
    quickSort(arr, low, pi - 1);
    quickSort(arr, pi + 1, high);
  }
}

int ObjQuickSorter::partition(int arr[], int low, int high) {
  int pivot = arr[high];
  int i = (low - 1);

  for (int j = low; j <= high - 1; j++) {
    if (arr[j] <= pivot) {
      i++;
      swap(&arr[i], &arr[j]);
    }
  }
  swap(&arr[i + 1], &arr[high]);
  return (i + 1);
}

void ObjQuickSorter::swap(int *a, int *b) {
  int t = *a;
  *a = *b;
  *b = t;
}
