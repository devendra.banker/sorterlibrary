#ifndef OBJINTSORTER_ALPHA_H
#define OBJINTSORTER_ALPHA_H

#include "ObjIntSorterFactory.h"
#include "ObjIntSorterStrategyProvider.h"
#include "ObjSorter_global.h"

namespace ObjSorterspace {
class ObjIntSorterAlpha : public IObjSorter {
 public:
  ObjIntSorterAlpha();
  int *sort(int arr[], int size, int &errorCode) override;
  void sortInteractive(int arr[], int size, int &errorCode) override;
  void setNextIterationFlag(bool nextIterationFlag) override;

 private:
  bool compare(int x, int y);
  int *inputArray_Int;
  int inputArraySize;
  ObjIntSorterStrategyProvider sp;
  IObjSorter *s;
};
}  // namespace ObjSorterspace
#endif
