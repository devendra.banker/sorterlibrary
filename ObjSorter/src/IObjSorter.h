#ifndef IOBJSORTER_H
#define IOBJSORTER_H

#include <QObject>

#include "ObjSorter_global.h"

namespace ObjSorterspace {

enum Sorter_State_t {
  SORTER_STATE_INIT = 0,
  SORTER_STATE_IN_PROGRESS = 1,
  SORTER_STATE_WAIT_FOR_ITERATION = 2,
  SORTER_STATE_COMPLETE = 3,
  SORTER_STATE_INVALID = -1
};

class IObjSorter : public QObject {
  Q_OBJECT

 public:
  virtual int *sort(int arr[], int size, int &errorCode) = 0;
  virtual void sortInteractive(int arr[], int size, int &errorCode) = 0;
  virtual void setNextIterationFlag(bool nextIterationflag) = 0;

 signals:
  void iterationStatus(int currentIteration, int index1, int index2,
                       bool swapFlag);
  void compareStatus(int currentIteration, int index1, int index2,
                     int outerLoopIndex);
  void sortStatus(ObjSorterspace::Sorter_State_t sorterState);
};
}  // namespace ObjSorterspace
Q_DECLARE_METATYPE(ObjSorterspace::Sorter_State_t)

#endif
