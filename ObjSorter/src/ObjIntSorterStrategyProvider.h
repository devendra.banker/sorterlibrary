#ifndef OBJINTSORTERSTRATEGYPROVIDER_H
#define OBJINTSORTERSTRATEGYPROVIDER_H

#include "IObjSorter.h"

namespace ObjSorterspace {
#define BUBBLE_SORT 1
#define QUICK_SORT 2

class ObjIntSorterStrategyProvider {
 public:
  IObjSorter *getSorterStrategy(int SorterType);
};
}  // namespace ObjSorterspace

#endif
