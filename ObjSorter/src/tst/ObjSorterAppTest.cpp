﻿#include <list>
#include "ObjIntSorterFactory.h"
#include "ObjSorter_global_test.h"
#include "gtest/gtest.h"

using namespace std;
using namespace testing;
using namespace ObjSorterspace;

bool CompareIntegerArrayList(int ActualSortedArray[], int ActualSortedArraySize,
                             int ExpectedSortedArray[],
                             int ExpectedSortedArraySize) {
  if (ActualSortedArraySize != ExpectedSortedArraySize) {
    return false;
  }
  for (int i = 0; i < ActualSortedArraySize; i++) {
    EXPECT_EQ(ActualSortedArray[i], ExpectedSortedArray[i])
        << "Vectors x and y differ at index " << i;
  }
  return true;
}

TEST(ObjSorterAppTestSuite, SortingForNull) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_TWO_NUMBERS] = {200, 2};
  int inputArray_Int_Size = 0;
  int *ActualSortedArray;
  int ExpectedSortedArray[SORTER_SIZE_TEST_TWO_NUMBERS] = {2, 200};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);

  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray, 0));
}

TEST(ObjSorterAppTestSuite, SortingForOneNumberTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_ONE_NUMBER] = {200};
  int inputArray_Int_Size = 1;
  int *ActualSortedArray;
  int ExpectedSortedArray[SORTER_SIZE_TEST_ONE_NUMBER] = {200};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);

  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray, 1));
}

TEST(ObjSorterAppTestSuite, SortingForTwoNumbersTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_TWO_NUMBERS] = {200, 2};
  int inputArray_Int_Size = 2;
  int *ActualSortedArray;
  int ExpectedSortedArray[SORTER_SIZE_TEST_TWO_NUMBERS] = {2, 200};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);

  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray, 2));
}

TEST(ObjSorterAppTestSuite, SortingForTwoNumbersAlreadySortedTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_TWO_NUMBERS] = {2, 200};
  int inputArray_Int_Size = 2;
  int *ActualSortedArray;
  int ExpectedSortedArray[SORTER_SIZE_TEST_TWO_NUMBERS] = {2, 200};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);

  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray, 2));
}

TEST(ObjSorterAppTestSuite, SortingForTenNumberTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_TEN_NUMBERS] = {2, 200, 400, 200, 211,
                                                      2, 2,   233, 432, 200};
  int inputArray_Int_Size = 10;
  int *ActualSortedArray;
  int ExpectedSortedArray[SORTER_SIZE_TEST_TEN_NUMBERS] = {
      2, 2, 2, 200, 200, 200, 211, 233, 400, 432};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);

  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray,
                                      inputArray_Int_Size));
}

TEST(ObjSorterAppTestSuite, SortingForHundredNumberTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_HUNDRED_NUMBERS] = {
      232, 64, 71, 77, 62, 50, 18, 65, 97, 69,  91, 14, 24, 1,  40, 29, 22,
      79,  4,  21, 49, 63, 82, 84, 93, 89, 28,  3,  46, 74, 25, 99, 59, 96,
      94,  61, 16, 38, 34, 39, 60, 33, 48, 7,   45, 27, 11, 92, 53, 95, 67,
      88,  72, 81, 68, 26, 85, 44, 98, 2,  31,  13, 83, 56, 8,  15, 5,  70,
      19,  51, 76, 86, 10, 23, 75, 47, 6,  17,  37, 43, 30, 78, 90, 20, 66,
      87,  58, 52, 35, 41, 36, 55, 42, 80, 100, 54, 12, 73, 57, 9};
  int inputArray_Int_Size = 100;
  int *ActualSortedArray;

  int ExpectedSortedArray[SORTER_SIZE_TEST_HUNDRED_NUMBERS] = {
      1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14,  15, 16, 17,
      18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,  33, 34, 35,
      36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,  50, 51, 52,
      53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66,  67, 68, 69,
      70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83,  84, 85, 86,
      87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 232};

  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  EXPECT_EQ(errorCode, SORTER_SUCCESS);
  ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                      ExpectedSortedArray,
                                      inputArray_Int_Size));
}

TEST(ObjSorterAppTestSuite, SortingThousandNumbersTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_THOUSAND_NUMBERS];
  list<int> l;
  for (int i = 0; i < SORTER_SIZE_TEST_THOUSAND_NUMBERS; i++) {
    inputArray_Int[i] = rand() % SORTER_SIZE_TEST_THOUSAND_NUMBERS;
    l.push_back((int)inputArray_Int[i]);
  }

  int inputArray_Int_Size = SORTER_SIZE_TEST_THOUSAND_NUMBERS;
  int *ActualSortedArray;
  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  l.sort();
  EXPECT_EQ(errorCode, SORTER_SUCCESS);
  if (errorCode == SORTER_SUCCESS) {
    int ExpectedSortedArray[SORTER_SIZE_TEST_THOUSAND_NUMBERS];

    list<int>::iterator it;
    int i = 0;
    for (it = l.begin(); it != l.end(); ++it) {
      ExpectedSortedArray[i] = (int)*it;
      i++;
    }
    ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                        ExpectedSortedArray,
                                        inputArray_Int_Size));
  }
}

TEST(ObjSorterAppTestSuite, SortingTenThousandNumbersTest) {
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_TEN_THOUSAND_NUMBERS];
  list<int> l;
  for (int i = 0; i < SORTER_SIZE_TEST_TEN_THOUSAND_NUMBERS; i++) {
    inputArray_Int[i] = rand() % SORTER_SIZE_TEST_TEN_THOUSAND_NUMBERS;
    l.push_back((int)inputArray_Int[i]);
  }

  int inputArray_Int_Size = SORTER_SIZE_TEST_TEN_THOUSAND_NUMBERS;
  int *ActualSortedArray;
  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  l.sort();

  EXPECT_EQ(errorCode, SORTER_SUCCESS);
  if (errorCode == SORTER_SUCCESS) {
    int ExpectedSortedArray[SORTER_SIZE_TEST_TEN_THOUSAND_NUMBERS];
    list<int>::iterator it;
    int i = 0;
    for (it = l.begin(); it != l.end(); ++it) {
      ExpectedSortedArray[i] = (int)*it;
      i++;
    }
    ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                        ExpectedSortedArray,
                                        inputArray_Int_Size));
  }
}

TEST(ObjSorterAppTestSuite,
     DISABLED_SortingHundredThousandNumbersTest)  // Bublesort limitation of
                                                  // >=100*1000 numbers
{
  ObjIntSorterFactory sf;
  IObjSorter *s = sf.CreateNew(SORTER_ALPHA_VERSION);

  int inputArray_Int[SORTER_SIZE_TEST_HUNDRED_THOUSAND_NUMBERS];
  list<int> l;
  for (int i = 0; i < SORTER_SIZE_TEST_HUNDRED_THOUSAND_NUMBERS; i++) {
    inputArray_Int[i] = rand() % SORTER_SIZE_TEST_HUNDRED_THOUSAND_NUMBERS;
    l.push_back((int)inputArray_Int[i]);
  }

  int inputArray_Int_Size = SORTER_SIZE_TEST_HUNDRED_THOUSAND_NUMBERS;
  int *ActualSortedArray;
  int errorCode = SORTER_INVALID;
  ActualSortedArray = s->sort(inputArray_Int, inputArray_Int_Size, errorCode);
  l.sort();

  EXPECT_EQ(errorCode, SORTER_SUCCESS);
  if (errorCode == SORTER_SUCCESS) {
    int ExpectedSortedArray[SORTER_SIZE_TEST_HUNDRED_THOUSAND_NUMBERS];

    list<int>::iterator it;
    int i = 0;
    for (it = l.begin(); it != l.end(); ++it) {
      ExpectedSortedArray[i] = (int)*it;
      i++;
    }

    ASSERT_TRUE(CompareIntegerArrayList(ActualSortedArray, inputArray_Int_Size,
                                        ExpectedSortedArray,
                                        inputArray_Int_Size));
  }
}
