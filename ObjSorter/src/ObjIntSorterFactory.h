#ifndef OBJINTSORTERFACTORY_H
#define OBJINTSORTERFACTORY_H

#include "IObjSorter.h"

namespace ObjSorterspace {
class ObjIntSorterFactory {
 public:
  IObjSorter *CreateNew(int Type);
};
}  // namespace ObjSorterspace
#endif
