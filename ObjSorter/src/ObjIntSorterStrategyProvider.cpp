#include "ObjIntSorterStrategyProvider.h"

#include <iostream>

#include "ObjBubbleSorter.h"
#include "ObjQuickSorter.h"

using namespace ObjSorterspace;

IObjSorter *ObjIntSorterStrategyProvider::getSorterStrategy(int SorterType) {
  if (SorterType == BUBBLE_SORT) {
    return new ObjBubbleSorter();
  } else if (SorterType == QUICK_SORT) {
    return new ObjQuickSorter();
  } else {
    return NULL;
  }
}
