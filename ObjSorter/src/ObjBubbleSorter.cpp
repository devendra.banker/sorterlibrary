#include "ObjBubbleSorter.h"

#include <iostream>

using namespace std;
using namespace ObjSorterspace;

ObjBubbleSorter::ObjBubbleSorter()
    : inputArray_Int(nullptr),
      inputArraySize(0),
      currentIteration(0),
      swapFlag(false),
      nextIterationFlag(false),
      outerLoopIndex(0),
      innerLoopIndex(0) {
  qRegisterMetaType<ObjSorterspace::Sorter_State_t>(
      "ObjSorterspace::Sorter_State_t");
  connect(&Timer, SIGNAL(timeout()), this, SLOT(OnTimeout()));
}

int *ObjBubbleSorter::sort(int arr[], int size, int &errorCode) {
  errorCode = SORTER_SUCCESS;

  if (size > SORTER_SIZE) {
    errorCode = SORTER_FAILURE_SIZE_OVERFLOW;
    inputArray_Int = NULL;
  } else {
    inputArray_Int = (int *)malloc(sizeof(int) * size);
    bool swap;
    do {
      int temp;
      swap = false;
      for (int x = 0; x < size - 1; x++) {
        if (compare(arr[x], arr[x + 1])) {
          temp = arr[x];
          arr[x] = arr[x + 1];
          arr[x + 1] = temp;
          swap = true;
        }
      }
    } while (swap);

    for (int i = 0; i < size; i++) {
      inputArray_Int[i] = arr[i];
    }

    inputArraySize = size;
  }

  return inputArray_Int;
}

void ObjBubbleSorter::sortInteractive(int arr[], int size, int &errorCode) {
  inputArraySize = size;

  errorCode = SORTER_SUCCESS;

  if (inputArraySize > SORTER_SIZE) {
    errorCode = SORTER_FAILURE_SIZE_OVERFLOW;
    inputArray_Int = NULL;
  } else {
    inputArray_Int = (int *)malloc(sizeof(int) * inputArraySize);
    for (int i = 0; i < inputArraySize; i++) {
      inputArray_Int[i] = arr[i];
    }
    if (currentIteration == 0) {
      Timer.start(1000);
      emit sortStatus(SORTER_STATE_INIT);
    }
  }
}

void ObjBubbleSorter::setNextIterationFlag(bool nextIterationFlag) {
  this->nextIterationFlag = nextIterationFlag;
}

void ObjBubbleSorter::OnTimeout() {
  if (nextIterationFlag == true) {
    if (outerLoopIndex < inputArraySize) {
      if (innerLoopIndex < inputArraySize - 1) {
        emit compareStatus(currentIteration, innerLoopIndex, innerLoopIndex + 1,
                           outerLoopIndex);
        if (compare(inputArray_Int[innerLoopIndex],
                    inputArray_Int[innerLoopIndex + 1])) {
          int temp;
          temp = inputArray_Int[innerLoopIndex];
          inputArray_Int[innerLoopIndex] = inputArray_Int[innerLoopIndex + 1];
          inputArray_Int[innerLoopIndex + 1] = temp;
          swapFlag = true;
          emit iterationStatus(currentIteration, innerLoopIndex,
                               innerLoopIndex + 1, swapFlag);
        }

        currentIteration++;
        innerLoopIndex++;
      } else {
        if (swapFlag == false) {
          Timer.stop();
          emit sortStatus(SORTER_STATE_COMPLETE);
        } else {
          swapFlag = false;
          innerLoopIndex = 0;
          outerLoopIndex++;
          emit sortStatus(SORTER_STATE_IN_PROGRESS);
        }
      }
    } else {
      Timer.stop();
      emit sortStatus(SORTER_STATE_COMPLETE);
    }
  } else {
    emit sortStatus(SORTER_STATE_WAIT_FOR_ITERATION);
  }
}

bool ObjBubbleSorter::compare(int x, int y) {
  if (x > y) {
    return true;
  } else {
    return false;
  }
}
