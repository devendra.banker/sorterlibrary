#ifndef OBJQUICKSORTER_H
#define OBJQUICKSORTER_H

#include "IObjSorter.h"
#include "ObjSorter_global.h"

namespace ObjSorterspace {
class ObjQuickSorter : public IObjSorter {
 public:
  ObjQuickSorter();
  int *sort(int arr[], int size, int &errorCode) override;
  void sortInteractive(int arr[], int size, int &errorCode) override;
  void setNextIterationFlag(bool nextIterationFlag) override;

 private:
  void quickSort(int arr[], int low, int high);
  int partition(int arr[], int low, int high);
  void swap(int *a, int *b);
  int *inputArray_Int;
  int inputArraySize;
  int currentIteration;
  bool nextIterationFlag;
};
}  // namespace ObjSorterspace
#endif
