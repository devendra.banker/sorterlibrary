#include "ObjIntSorterFactory.h"

#include <iostream>

#include "ObjIntSorterAlpha.h"

using namespace ObjSorterspace;

IObjSorter *ObjSorterspace::ObjIntSorterFactory::CreateNew(int Type) {
  if (Type == SORTER_ALPHA_VERSION) {
    return new ObjIntSorterAlpha();
  } else {
    return NULL;
  }
}
