Introduction
------------
    Welcome to Adaptive Software Library using C++, CMAKE, CI/CD, TDD and design principle for Sorting Integer Numbers.

Note:
-----
   GitLab CI/CD would not work due to QT dependency introduction to support signaling from library (Docker image needs to be updated) - TBD

Todo :
------
    - package bundling - https://dominikberner.ch/cmake-interface-lib/ 
    - Sorting Algorithms Visualization
    - Sorting Algorithms Performance Report
    - Sorting Algorithms Configuration Support
    - Clean Code Refactoring
    - ++ 

Video Demonstration 
-------------------
  - f21208bad90b433fa7304a427f50671a6d21e34f Commit : Software Trail Youtube Channel : https://www.youtube.com/watch?v=_es7ITLHGKU&t=18s

Concepts Included:
------------------
    - CMake
    - GoogleTest
    - Test Driven Development
    - Git Lab CI/CD
    - Open Close Principle
    - Single Responsibility Principle
    - Factory Pattern
    - Strategy Pattern
    - Bubble Sorting
    - Quick Sorting
    - Performance Evaluation of different sorting algorithms
    - Add Coverage Report and GitLab Badge

Building:
---------
    - Download
    - Run CMake
    - Build

Black Box Testing:
------------------
    - Build
    - Run Sorter_tst

Future Change Requirements Supported:
-------------------------------------
    - New Sorting Object addition
    - New Sorting Algorithm addition


